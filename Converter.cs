using System;
using System.Collections.Generic;

namespace Modelling
{

    public class Number // пусть этот класс будет одиночкой
    {
        string input;
        int result;
        bool thousandFlag = false;
        private static Number _instance;

        private Number(string input)
        {
            this.Input = input;
        }

        public static Number getInstance(string input)
        {
            if (_instance == null)
            {
                _instance = new Number(input);
            }
            return _instance;
        }


        public string Input
        {
            get { return input; }
            set { input = value; }
        }

        public int Result
        {
            get { return result; }
            set { result = value; }
        }

        public bool Flag
        {
            get { return thousandFlag; }
            set { thousandFlag = value; }
        }
    }

    public abstract class AbstractExpression
    {
        public void Interpret(Number number)
        {
            if (number.Input.Length == 0)
                return;

            if (number.Input.StartsWith("/"))
            {
                number.Flag = true;
                number.Input = number.Input.Substring(1);
                
            }
            else if (number.Input.StartsWith(Nine()))
            {
                number.Result += number.Flag == true ? (9 * digitMultiplier() * 1000) : (9 * digitMultiplier());
                number.Flag = false;
                number.Input = number.Input.Substring(1);
            }
            else if (number.Input.StartsWith(Eight()))
            {
                number.Result += number.Flag == true ? (8 * digitMultiplier() * 1000) : (8 * digitMultiplier());
                number.Flag = false;
                number.Input = number.Input.Substring(1);
            }
            else if (number.Input.StartsWith(Seven()))
            {
                number.Result += number.Flag == true ? (7 * digitMultiplier() * 1000) : (7 * digitMultiplier());
                number.Flag = false;
                number.Input = number.Input.Substring(1);
            }
            else if (number.Input.StartsWith(Six()))
            {
                number.Result += number.Flag == true ? (6 * digitMultiplier() * 1000) : (6 * digitMultiplier());
                number.Flag = false;
                number.Input = number.Input.Substring(1);
            }
            else if (number.Input.StartsWith(Four()))
            {
                number.Result += number.Flag == true ? (4 * digitMultiplier() * 1000) : (4 * digitMultiplier());
                number.Flag = false;
                number.Input = number.Input.Substring(1);
            }
            else if (number.Input.StartsWith(Five()))
            {
                number.Result += number.Flag == true ? (5 * digitMultiplier() * 1000)  : (5 * digitMultiplier());
                number.Flag = false;
                number.Input = number.Input.Substring(1);
            }
            else if (number.Input.StartsWith(Three()))
            {
                number.Result += number.Flag == true ? (3 * digitMultiplier() * 1000) : (3 * digitMultiplier());
                number.Flag = false;
                number.Input = number.Input.Substring(1);
            }
            else if (number.Input.StartsWith(Two()))
            {
                number.Result += number.Flag == true ? (2 * digitMultiplier() * 1000)  : (2 * digitMultiplier());
                number.Flag = false;
                number.Input = number.Input.Substring(1);
            }

            else if (number.Input.StartsWith(One()))
            {
                number.Result += number.Flag == true ? (1 * digitMultiplier() * 1000) :  (1 * digitMultiplier());
                number.Flag = false;
                number.Input = number.Input.Substring(1);
            }
        }

        public abstract string One();
        public abstract string Two();
        public abstract string Three();
        public abstract string Four();
        public abstract string Five();
        public abstract string Six();
        public abstract string Seven();
        public abstract string Eight();
        public abstract string Nine();

        public abstract int digitMultiplier();
    }

    
    public class ThousandDigit : AbstractExpression
    {
        public override string One() { return " "; }
        public override string Two() { return " "; }
        public override string Three() { return " "; }
        public override string Four() { return " "; }
        public override string Five() { return " "; }
        public override string Six() { return " "; }
        public override string Seven() { return " "; }
        public override string Eight() { return " "; }
        public override string Nine() { return " "; }
        public override int digitMultiplier() { return 1000; }
    } 


    public class HundredDigit : AbstractExpression
    {
        public override string One() { return "Р"; }
        public override string Two() { return "С"; }
        public override string Three() { return "Т"; }
        public override string Four() { return "У"; }
        public override string Five() { return "Ф"; }
        public override string Six() { return "Х"; }
        public override string Seven() { return "U"; }
        public override string Eight() { return "W"; }
        public override string Nine() { return "Ц"; }
        public override int digitMultiplier() { return 100; }
    }


    public class TenDigit : AbstractExpression
    {
        public override string One() { return "L"; }
        public override string Two() { return "К"; }
        public override string Three() { return "Л"; }
        public override string Four() { return "М"; }
        public override string Five() { return "Н"; }
        public override string Six() { return "Q"; }
        public override string Seven() { return "О"; }
        public override string Eight() { return "П"; }
        public override string Nine() { return "Ч"; }
        public override int digitMultiplier() { return 10; }
    }



    public class OneDigit : AbstractExpression
    {
        public override string One() { return "А"; }
        public override string Two() { return "В"; }
        public override string Three() { return "Г"; }
        public override string Four() { return "Д"; }
        public override string Five() { return "Е"; }
        public override string Six() { return "S"; }
        public override string Seven() { return "З"; }
        public override string Eight() { return "Ф"; }
        public override string Nine() { return "F"; }
        public override int digitMultiplier() { return 1; }
    }


    public class Program
    {
        public static void Main(string[] args)
        {
            string slavicNumber = "РКВ";
            Number number = Number.getInstance(slavicNumber);

            List<AbstractExpression> tree = new List<AbstractExpression>();
            tree.Add(new ThousandDigit());
            tree.Add(new HundredDigit());
            tree.Add(new TenDigit());
            tree.Add(new OneDigit());

            foreach (AbstractExpression exp in tree)
            {
                exp.Interpret(number);
            }

            Console.WriteLine($"Кириллическое {slavicNumber} равняется арабскому {number.Result}");
            // Вывод в консоль: Кириллическое РКВ равняется арабскому 122
        }
    }
}